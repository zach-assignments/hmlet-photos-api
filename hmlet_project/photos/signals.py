import os

from django.conf import settings
from django.db.models.signals import post_delete, post_save

from photos.models import Photo


def delete_remaining_image_file_from_storage(sender, **kwargs):
    """
    Delete the image file that remains in storage when a photo instance is deleted
    """
    photo_instance = kwargs.get("instance", None)

    if photo_instance:
        image_path = photo_instance.image.path
        image_abs_path = os.path.join(settings.BASE_DIR, image_path)
        thumbnail_path = photo_instance.thumbnail.path
        thumbnail_abs_path = os.path.join(settings.BASE_DIR, thumbnail_path)

        try:
            os.remove(image_abs_path)
            os.remove(thumbnail_abs_path)
        except:
            pass

        try:
            os.rmdir(os.path.dirname(image_abs_path))
            os.rmdir(os.path.dirname(thumbnail_abs_path))
        except (FileNotFoundError, OSError):
            # when the directory does not exist or is not empty
            pass


post_delete.connect(
    delete_remaining_image_file_from_storage,
    sender=Photo,
    weak=False,
    dispatch_uid="delete_remaining_image_file_from_storage_on_post_delete_photo",
)
