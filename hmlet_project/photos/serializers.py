from rest_framework import serializers

from photos.models import Photo


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = Photo
        fields = [
            "url",
            "id",
            "title",
            "caption",
            "created",
            "updated",
            "image",
            "thumbnail",
            "owner",
            "is_draft",
            "published",
        ]
