import os

from django.conf import settings
from django.contrib.auth.models import User
from django.core.files import temp as tempfile
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from PIL import Image

from photos.models import Photo


class PhotoIntegrationTests(APITestCase):
    def create_user(self):
        self.user = User.objects.create_user(
            "test_user", "test@test.com", password="test_password"
        )

    def create_existing_photo_object(self):
        self.create_user()

        file_1 = SimpleUploadedFile(
            "test_1.jpeg", b"some content", content_type="image/jpeg"
        )

        self.existing_photo = Photo.objects.create(
            title="first title",
            caption="first caption",
            image=file_1,
            owner_id=self.user.id,
        )

        # clean up dummy files and dir: addCleanUp() will be executed in reverse order
        # TODO: rmdirs not working because test runner and app code are different owners
        abs_image_path = os.path.join(settings.BASE_DIR, self.existing_photo.image.path)
        origin_dir = os.path.dirname(abs_image_path)
        self.addCleanup(
            os.rmdir, os.path.dirname(origin_dir)
        )  # remove media/user_1 dir
        self.addCleanup(os.rmdir, origin_dir)  # remove media/user_1/origin dir
        self.addCleanup(os.remove, abs_image_path)

    def create_payload_file_with_image(self):
        file_obj = tempfile.NamedTemporaryFile(suffix=".jpg")
        image = Image.new("RGB", (50, 50))
        image.save(file_obj, "JPEG")

        self.payload_file_obj = file_obj
        self.image = image

    def setUp(self):
        self.create_existing_photo_object()  # calls create_user in method
        self.create_payload_file_with_image()

        # authenticate user for sending authenticated requests
        self.client.force_authenticate(user=self.user)

    def test_create_photo_POST_request(self):
        """
        Photo instance can be created and persisted in DB through POST request
        """
        with open(self.payload_file_obj.name, "rb") as image_data:
            data = {
                "title": "Test title",
                "caption": "Test captions",
                "image": image_data,
            }

            url = reverse("photo-list")

            response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Photo.objects.count(), 2)
        self.assertEqual(Photo.objects.get(id=2).title, "Test title")

        # clean up dummy files and dir
        photo_to_delete = Photo.objects.get(id=2)
        self.addCleanup(
            os.remove, os.path.join(settings.BASE_DIR, photo_to_delete.image.path)
        )
        self.addCleanup(
            os.remove, os.path.join(settings.BASE_DIR, photo_to_delete.thumbnail.path)
        )

    def test_update_photo_caption_PATCH_request(self):
        """
        Photo instance caption can be updated
        """
        data = {"title": "Test title", "caption": "Test captions"}

        url = "/photos/1/"
        response = self.client.patch(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(Photo.objects.get(id=1).title, "first title")
        self.assertEqual(Photo.objects.get(id=1).caption, "Test captions")

    def test_update_photo_image_PATCH_request_should_fail(self):
        """
        Photo instance image should not be updated
        """
        with open(self.payload_file_obj.name, "rb") as image_data:
            data = {
                "caption": "Test captions",
                "image": image_data,
            }

            url = "/photos/1/"
            response = self.client.patch(url, data)

        photo_to_inspect = Photo.objects.get(id=1)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(photo_to_inspect.title, "first title")
        self.assertNotEqual(photo_to_inspect.caption, "Test caption")
        self.assertEqual(photo_to_inspect.image.name, self.existing_photo.image.name)

    def test_update_photo_PUT_request_should_fail(self):
        """
        Photo instance should not be updated
        when PUT request is used
        """
        with open(self.payload_file_obj.name, "rb") as image_data:
            data = {
                "title": "Test title",
                "caption": "Test captions",
                "image": image_data,
            }

            url = "/photos/1/"
            response = self.client.put(url, data)

        photo_to_inspect = Photo.objects.get(id=1)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertNotEqual(photo_to_inspect.title, "Test title")
        self.assertNotEqual(photo_to_inspect.caption, "Test caption")
        self.assertEqual(photo_to_inspect.image.name, self.existing_photo.image.name)
