import os
from unittest import mock

from django.conf import settings
from django.test import TestCase

from photos.signals import delete_remaining_image_file_from_storage


class PhotoReceiverTests(TestCase):
    def setUp(self):
        self.mock_dir = mock.Mock()
        self.mock_dir.name = os.path.join(settings.BASE_DIR, "mock_dir")

    @mock.patch("os.remove")
    @mock.patch("os.rmdir")
    def test_receiver_delete_remaining_image_file_from_storage(
        self, patch_rmdir, patch_remove
    ):
        """
        Image file is deleted from storage
        Thumbnail file is deleted from storage
        """
        mock_instance = mock.Mock()
        mock_instance.image.path = os.path.join(self.mock_dir.name, "imagexyz")
        mock_instance.thumbnail.path = os.path.join(self.mock_dir.name, "thumbnailxyz")

        delete_remaining_image_file_from_storage(None, instance=mock_instance)

        patch_remove.assert_has_calls(
            [
                mock.call(mock_instance.image.path),
                mock.call(mock_instance.thumbnail.path),
            ]
        )
        patch_rmdir.assert_called_with(self.mock_dir.name)
