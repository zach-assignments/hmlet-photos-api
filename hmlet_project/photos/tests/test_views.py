from unittest import mock

from django.contrib.auth.models import AnonymousUser, User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from rest_framework import status

from photos.models import Photo
from photos.views import PhotoViewSet


class PhotoViewTests(TestCase):
    def create_users(self):
        User.objects.create_user("first_user", "password123")
        User.objects.create_user("second_user", "password123")

    def create_photos(self):
        Photo.objects.create(
            title="first_user_published_photo", owner_id=1, is_draft=False
        )
        Photo.objects.create(title="first_user_draft_photo", owner_id=1, is_draft=True)
        Photo.objects.create(
            title="second_user_published_photo", owner_id=2, is_draft=False
        )
        Photo.objects.create(title="second_user_draft_photo", owner_id=2, is_draft=True)

    def setUp(self):
        self.create_users()
        self.create_photos()
        self.viewset = PhotoViewSet()

    def test_get_queryset(self):
        """
        get_queryset should return different querysets
        For authenticated users: user's own photo objects
        For non-auth users: all published photo objects
        """
        # set up for auth user
        request = mock.Mock()
        request.user = User.objects.get(id=1)
        self.viewset.request = request

        queryset = self.viewset.get_queryset()

        self.assertEqual(
            queryset.explain(), Photo.objects.filter(owner=request.user).explain()
        )

        # set up for non-auth user
        self.viewset.request.user = AnonymousUser()

        queryset = self.viewset.get_queryset()
        self.assertEqual(
            queryset.explain(), Photo.objects.filter(is_draft=False).explain()
        )

    @mock.patch("photos.services.ImageService.create_thumbnail_from_uploaded_file")
    def test_perform_create(self, mock_create_thumbnail):
        """
        perform_create should save instance with owner and thumbnail
        during instance creation
        """

        request = mock.Mock()
        request.user = User.objects.get(id=1)
        self.viewset.request = request
        mock_create_thumbnail.return_value = "thumbnail"

        serializer = mock.Mock()

        self.viewset.perform_create(serializer)

        serializer.save.assert_called_once()
        serializer.save.assert_called_with(owner=request.user, thumbnail="thumbnail")

    def test_update_fail(self):
        """
        update should be blocked if an image_file is included
        """
        request = mock.Mock()
        request.data = {}
        request.data["image"] = SimpleUploadedFile(
            "test_1.jpeg", b"some content", content_type="image/jpeg"
        )

        self.viewset.request = request

        response = self.viewset.partial_update(request)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @mock.patch("rest_framework.viewsets.ModelViewSet.partial_update")
    def test_update(self, patch_super_partial_update):
        """
        update should pass if there is no image file
        """
        request = mock.Mock()
        request.data = {}

        self.viewset.request = request

        response = self.viewset.partial_update(request)

        patch_super_partial_update.assert_called_once()

    def test_non_partial_update(self):
        """
        update should fail if it is not a partial update
        """
        request = mock.Mock()

        response = self.viewset.update(request, {})

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_action_all_published_photos(self):
        """
        all() should return all published photos
        """
        # mock filter_queryset, paginate_queryset, get_serializer, get_paginated_response..etc.
        # using magic mock???

        # response = self.viewset.all()

        pass
